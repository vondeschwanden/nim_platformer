import
  sdl2, sdl2.ttf,
  strutils,
  tables,

  utils, renderable

type
  Glyph* = ref object of Renderable
    font*: FontPtr
    fillColor*, strokeColor*: Color
    strokeWidth*: int
    offset*: tuple[x, y: int]
    c*, cached_c: char


proc newGlyph*(font: FontPtr, fillColor, strokeColor: Color, strokeWidth: int, c: char, offset: tuple[x, y: int]): Glyph = 
  new result
  result.scale = 1
  result.font = font
  result.fillColor = fillColor
  result.strokeColor = strokeColor
  result.strokeWidth = strokeWidth
  result.c = c
  result.offset = offset
  result.cached_c = char(0)

proc render_internal(c: char, font: FontPtr, color: Color, strokeWidth: int, renderer: RendererPtr): tuple[texture: TexturePtr, width, height: cint] =
  # glyph.font.setFontHinting(1)
  font.setFontOutline(strokeWidth.cint)
  var glyphSurface = font.renderUtf8Blended($c, color)
  sdlFailIf glyphSurface.isNil: "Could not render text surface"
  
  discard glyphSurface.setSurfaceAlphaMod(color.a)

  result.width = glyphSurface.w
  result.height = glyphSurface.h

  result.texture = renderer.createTextureFromSurface(glyphSurface)
  sdlFailIf result.texture.isNil: "Could not create texture from rendered text"

  glyphSurface.freeSurface()

proc render_fill(glyph: Glyph, renderer: RendererPtr): tuple[texture: TexturePtr, width, height: cint] =
  return render_internal(glyph.c, glyph.font, glyph.fillColor, 0, renderer)

proc render_stroke(glyph: Glyph, renderer: RendererPtr): tuple[texture: TexturePtr, width, height: cint] =
  return render_internal(glyph.c, glyph.font, glyph.strokeColor, glyph.strokeWidth, renderer)

method is_dirty*(glyph: Glyph): bool = 
  glyph.cached_c != glyph.c

method render*(glyph: Glyph, renderer: RendererPtr) =
  glyph.cached_c = glyph.c

  echo "Rendering Glyph '$#'".format(glyph.c)

  # Render fill
  var fillTexture: TexturePtr
  var fillTextureWidth, fillTextureHeight: cint
  (fillTexture, fillTextureWidth, fillTextureHeight) = glyph.render_fill(renderer)

  # Render stroke
  var strokeTexture: TexturePtr
  var strokeTextureWidth, strokeTextureHeight: cint
  if glyph.strokeWidth > 0:
    (strokeTexture, strokeTextureWidth, strokeTextureHeight) = glyph.render_stroke(renderer)

  glyph.width = max(fillTextureWidth, strokeTextureWidth)
  glyph.height = max(fillTextureHeight, strokeTextureHeight)

  # Create glyph texture
  glyph.texture.destroy()
  glyph.texture = renderer.createTexture(SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, glyph.width.cint, glyph.height.cint)
  discard glyph.texture.setTextureBlendMode(BlendMode_None)
  renderer.withRenderTarget(glyph.texture):
    if glyph.strokeWidth > 0:
      var r = rect(0, 0, strokeTextureWidth, strokeTextureHeight)
      renderer.copyEx(strokeTexture, r, r, angle = 0.0, center = nil)
    var src = rect(0, 0, fillTextureWidth, fillTextureHeight)
    var dst = rect((glyph.strokeWidth + glyph.offset.x).cint, (glyph.strokeWidth + glyph.offset.y).cint, fillTextureWidth, fillTextureHeight)
    renderer.copyEx(fillTexture, src, dst, angle = 0.0, center = nil)
