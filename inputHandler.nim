import
  sdl2

type
  Input* {.pure.} = enum none, quit, left, right, up, down, blur, reset

  InputHandler* = ref object of RootObj
    inputs*: array[Input, bool]

proc newInputHandler*(): InputHandler = 
  new result

proc toInput*(key: Scancode): Input =
  case key
  of SDL_SCANCODE_Q: Input.quit
  of SDL_SCANCODE_LEFT: Input.left
  of SDL_SCANCODE_RIGHT: Input.right
  of SDL_SCANCODE_UP: Input.up
  of SDL_SCANCODE_DOWN: Input.down
  of SDL_SCANCODE_B: Input.blur
  of SDL_SCANCODE_R: Input.reset
  else: Input.none

proc handleInput*(handler: InputHandler) =
  var event = defaultEvent
  while pollEvent(event):
    case event.kind
    of QuitEvent:
      handler.inputs[Input.quit] = true
    of KeyDown:
      handler.inputs[event.key.keysym.scancode.toInput] = true
    of KeyUp:
      handler.inputs[event.key.keysym.scancode.toInput] = false
    else:
      discard
