import
  sdl2, 

  renderable


type
  Sprite* = ref object of Renderable
    name: string


proc newSprite*(texture: TexturePtr, x, y, width, height: int, scale: float): Sprite = 
  new result
  result.texture = texture
  result.x = x
  result.y = y
  result.width = width
  result.height = height
  result.scale = scale

method is_dirty*(sprite: Sprite): bool =
  false

method render*(sprite: Sprite, renderer: RendererPtr) = 
  discard
