import
  sdl2, sdl2.image, sdl2.ttf,
  basic2d, strutils, times, math, strfmt, os, streams

type
  SDLException = object of Exception

template sdlFailIf*(cond: typed, reason: string) =
  if cond: raise SDLException.newException(
    reason & ", SDL error: " & $getError())

const dataDir = "data"

when defined(embedData):
  template readRW*(filename: string): ptr RWops =
    const file = staticRead(dataDir / filename)
    rwFromConstMem(file.cstring, file.len)

  template readStream*(filename: string): Stream =
    const file = staticRead(dataDir / filename)
    newStringStream(file)
else:
  let fullDataDir = getAppDir() / dataDir

  template readRW*(filename: string): ptr RWops =
    var rw = rwFromFile(cstring(fullDataDir / filename), "r")
    sdlFailIf rw.isNil: "Cannot create RWops from file"
    rw


template withRenderTarget*(renderer: RendererPtr, texture: TexturePtr, actions: untyped): untyped =
  var bkp_target = renderer.getRenderTarget()
  renderer.setRenderTarget(texture)
  actions
  renderer.setRenderTarget(bkp_target)
