import
  sdl2, basic2d,

  renderable

type  
  Scene* = ref object of RootObj
    camera*: Vector2d
    objects: ref seq[Renderable]

proc add*(scene: Scene, renderable: Renderable) = 
  scene.objects[].add(renderable)

proc render*(scene: Scene, renderer: RendererPtr) =
  # Draw over all drawings of the last frame with the default color
  renderer.clear()

  for obj in scene.objects[]:
    if obj.is_dirty:
      obj.render(renderer)

    var source = rect(0, 0, obj.width.cint, obj.height.cint)
    var dest = rect(obj.x.cint, obj.y.cint, (float(obj.width) * obj.scale).cint, (float(obj.height) * obj.scale).cint)
    
    renderer.copyEx(obj.texture, source, dest, angle = 0.0, center = nil)

  # Show the result on screen
  renderer.present()


proc newScene*(): Scene =
  new result
  result.objects = new seq[Renderable]
  result.objects[] = @[]
