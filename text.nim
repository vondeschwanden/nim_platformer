import
  sdl2, sdl2.ttf,

  glyphcache, utils, glyph, renderable


type
  Text* = ref object of Renderable
    font: FontPtr
    fillColor, strokeColor: Color
    strokeWidth: int
    text: string
    cached_text: string
    glyphcache: GlyphCache

const
  maxwidth = 1920
  maxheight = 1080

proc newText*(text: string, x, y: int, font: FontPtr, fillColor: Color, strokeColor: Color, strokeWidth: int, glyphcache: GlyphCache): Text = 
  new result
  result.scale = 1
  result.text = text
  result.cached_text = nil
  result.x = x
  result.y = y
  result.font = font
  result.fillColor = fillColor
  result.strokeColor = strokeColor
  result.strokeWidth = strokeWidth
  result.glyphcache = glyphcache
  sdlFailIf result.font.isNil: "Failed to load font"

method is_dirty*(text: Text): bool =
  text.text != text.cached_text

method render*(text: Text, renderer: RendererPtr) = 
  echo "Rendering Text '$1'" % [text.text]
  text.cached_text = text.text
  text.texture.destroy()
  text.texture = renderer.createTexture(SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, maxwidth, maxheight)

  discard text.texture.setTextureBlendMode(BlendMode_Blend)
  
  text.width = 0
  text.height = 0

  renderer.withRenderTarget(text.texture):
    for i in 0..<text.text.len:
      var glyph = text.glyphCache.get(renderer, text.font, text.fillColor, text.strokeColor, text.strokeWidth, text.text[i])
      glyph.x = text.width
      text.width += glyph.width
      text.height = max(text.height, glyph.height)

      var source = rect(0, 0, glyph.width.cint, glyph.height.cint)
      var dest = rect(glyph.x.cint, glyph.y.cint, glyph.width.cint, glyph.height.cint)

      renderer.copyEx(glyph.texture, source, dest, angle = 0.0, center = nil)
