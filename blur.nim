import
  sdl2,
  renderable, effect


const
  b = 0
  g = 1
  r = 2
  # a = 3

type 
  BlurEffect* = ref object of Effect
    name: string

proc newBlurEffect*(renderable: Renderable, renderer: RendererPtr): BlurEffect = 
  new result
  result.init(renderable, renderer)

proc applyBoxKernel(source: ref array[maxwidth * maxheight * bitwidth, uint8], center, width: int): uint8 {.inline.} = 
  var avg = (float(source[][center - (width + 1) * bitwidth]) + float(source[][center - width * bitwidth]) + float(source[][center - (width - 1) * bitwidth]))
  avg += (float(source[][center - bitwidth]) + float(source[][center]) + float(source[][center + bitwidth]))
  avg += (float(source[][center + (width - 1) * bitwidth]) + float(source[][center + width * bitwidth]) + float(source[][center + (width + 1) * bitwidth]))
  return uint8(avg / 9.0)

proc applyLineKernel(source: ref array[maxwidth * maxheight * bitwidth, uint8], center: int): uint8 {.inline.} = 
  return uint8(float(source[][center - bitwidth]) * 0.25 + float(source[][center]) * 0.5 + float(source[][center + bitwidth]) * 0.25)

proc applyColumnKernel(source: ref array[maxwidth * maxheight * bitwidth, uint8], center, width: int): uint8 {.inline.} = 
  return uint8(float(source[][center - width * bitwidth]) * 0.25 + float(source[][center]) * 0.5 + float(source[][center + width * bitwidth]) * 0.25)

proc boxBlur*(effect: BlurEffect, renderable: Renderable, renderer: RendererPtr) = 
  let width = renderable.width + margin
     
  for y in margin..<renderable.height:
    for x in margin..<renderable.width:
      let offset = (x + y * width) * bitwidth
      # effect.dest[][offset + a] = applyBoxKernel(effect.source, offset + a, width) # a
      effect.dest[][offset + r] = applyBoxKernel(effect.source, offset + r, width) # r
      effect.dest[][offset + g] = applyBoxKernel(effect.source, offset + g, width) # g
      effect.dest[][offset + b] = applyBoxKernel(effect.source, offset + b, width) # b

  # swap dest and source
  var tmp = effect.dest
  effect.dest = effect.source
  effect.source = tmp
  
  discard updateTexture(renderable.texture, nil, cast[pointer](effect.source), (width * bitwidth).cint)

method apply*(effect: BlurEffect, area: Rect, renderer: RendererPtr) = 
  let width = effect.renderable.width + margin

  for y in area.y - 1..<area.y + area.h + 1:
    for x in area.x..<area.x + area.w:
      let offset = (x + margin + (y + margin) * width) * bitwidth
      # effect.dest[][offset + a] = applyLineKernel(effect.source, offset + a) # a
      effect.dest[][offset + r] = applyLineKernel(effect.source, offset + r) # r
      effect.dest[][offset + g] = applyLineKernel(effect.source, offset + g) # g
      effect.dest[][offset + b] = applyLineKernel(effect.source, offset + b) # b

  for y in area.y..<area.y + area.h:
    for x in area.x..<area.x + area.w:
      let offset = (x + margin + (y + margin) * width) * bitwidth
      # effect.source[][offset + a] = applyColumnKernel(effect.dest, offset + a, width) # a
      effect.source[][offset + r] = applyColumnKernel(effect.dest, offset + r, width) # r
      effect.source[][offset + g] = applyColumnKernel(effect.dest, offset + g, width) # g
      effect.source[][offset + b] = applyColumnKernel(effect.dest, offset + b, width) # b

  discard updateTexture(effect.renderable.texture, nil, cast[pointer](effect.source), (width * bitwidth).cint)
