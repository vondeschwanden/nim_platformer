import
  sdl2,

  renderable


const
  bitwidth* = 4
  margin* = 50
  maxwidth* = 1920 + 2 * margin
  maxheight* = 1080 + 2 * margin


type 
  Effect* = ref object of RootObj
    renderable*: Renderable
    source*, dest*: ref array[maxwidth * maxheight * bitwidth, uint8]
    format*: uint32
    original*: TexturePtr
    rect*: Rect


proc makeDrawable(effect: Effect, renderable: Renderable, renderer: RendererPtr) = 
  effect.rect = rect(0, 0, renderable.width.cint, renderable.height.cint)
  var re2 = rect(margin, margin, renderable.width.cint, renderable.height.cint)
  var re3 = rect(0, 0, (renderable.width + 2 * margin).cint, (renderable.height + 2 * margin).cint)
  discard queryTexture(renderable.texture, addr(effect.format), nil, nil, nil)

  var tmp_texture = renderer.createTexture(effect.format, SDL_TEXTUREACCESS_TARGET, (renderable.width + 2 * margin).cint, (renderable.height + 2 * margin).cint)
  
  # Shift renderable texture by margin
  renderer.setRenderTarget(tmp_texture)
  renderer.copyEx(renderable.texture, effect.rect, re2, angle = 0.0, center = nil)

  # Read all renderable texture pixels (with margin) into data array
  discard renderer.readPixels(re3, effect.format.cint, cast[pointer](effect.source), ((renderable.width + margin * 2) * bitwidth).cint)
  renderer.setRenderTarget(nil)

  effect.original = renderer.createTexture(effect.format, SDL_TEXTUREACCESS_TARGET, (renderable.width + 2 * margin).cint, (renderable.height + 2 * margin).cint)
  renderer.setRenderTarget(effect.original)
  renderer.copyEx(renderable.texture, effect.rect, re2, angle = 0.0, center = nil)
  renderer.setRenderTarget(nil)

  renderable.texture = tmp_texture
  renderable.x -= margin
  renderable.y -= margin
  renderable.width += margin
  renderable.height += margin

proc init*(effect: Effect, renderable: Renderable, renderer: RendererPtr) = 
  effect.renderable = renderable
  effect.source = new array[maxwidth * maxheight * bitwidth, uint8]
  effect.dest = new array[maxwidth * maxheight * bitwidth, uint8]

  effect.makeDrawable(renderable, renderer)

proc reset*(effect: Effect, renderer: RendererPtr) = 
  let width = effect.renderable.width + margin
  var re3 = rect(0, 0, width.cint, (effect.renderable.height + margin).cint)
  # read data from original
  renderer.setRenderTarget(effect.original)
  discard renderer.readPixels(re3, effect.format.cint, cast[pointer](effect.source), (width * bitwidth).cint)
  renderer.setRenderTarget(nil)

  # update texture
  discard updateTexture(effect.renderable.texture, nil, cast[pointer](effect.source), (width * bitwidth).cint)

method apply*(effect: Effect, area: Rect, renderer: RendererPtr) {.base.} = 
  quit "to override!"

proc apply*(effect: Effect, renderer: RendererPtr) = 
  effect.apply(effect.rect, renderer)
