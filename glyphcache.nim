import
  sdl2, sdl2.ttf,
  tables,

  glyph

type  
  GlyphCache* = ref object of RootObj
    cache: ref seq[Glyph]

const
  offsets: Table[char, tuple[x, y: int]] = {
    'v': (x: 1, y: 0),
    'w': (x: 1, y: 0),
    'y': (x: 1, y: 0),
    'V': (x: 1, y: 0),
    'W': (x: 1, y: 0),
    'Y': (x: 1, y: 0),
    ')': (x: 1, y: 0),
    '<': (x: 0, y: 1),
    }.toTable

proc getOffset(glyph: char): tuple[x, y: int] =
  if not offsets.hasKey(glyph):
    return (0, 0)
  else:
    return (offsets[glyph].x, offsets[glyph].y)

proc newGlyphCache*(): GlyphCache =
  new result
  result.cache = new seq[Glyph]
  result.cache[] = @[]

proc get*(glyphCache: GlyphCache, renderer: RendererPtr, font: FontPtr, fillColor, strokeColor: Color, strokeWidth: int, c: char): Glyph = 
  result = nil
  for g in glyphCache.cache[]:
    if g.font == font and g.fillColor == fillColor and g.strokeColor == strokeColor and g.strokeWidth == strokeWidth and g.c == c:
      result = g
  
  if result == nil:
    result = newGlyph(font, fillColor, strokeColor, strokeWidth, c, getOffset(c))
    glyphCache.cache[].add(result)

  if result.is_dirty:
    result.render(renderer)
  return result
