import
  sdl2.ttf,
  tables,

  utils

type  
  FontCache* = ref object of RootObj
    cache: Table[tuple[name: string, size: int], FontPtr]

proc newFontcache*(): FontCache = 
  new result
  result.cache = initTable[tuple[name: string, size: int], FontPtr]()


proc get*(fontCache: FontCache, name: string, size: int): FontPtr = 
  let k = (name, size)
  if not fontCache.cache.hasKey(k):
    fontCache.cache.add(k, openFontRW(readRW(name), freesrc = 1, size.cint))
  result = fontCache.cache[k]
