import
  sdl2, sdl2.image, sdl2.ttf,
  basic2d, strutils, times, math, strfmt, os, streams,
  tables,

  scene, text, utils, inputHandler, sprite, fontcache, glyphcache, effect, blur, glyph

const
  windowSize: Point = (1800.cint, 1000.cint)

proc main =
  sdlFailIf(not sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)):
    "SDL2 initialization failed"

  # defer blocks get called at the end of the procedure, even if an
  # exception has been thrown
  defer: sdl2.quit()

  sdlFailIf(not setHint("SDL_RENDER_SCALE_QUALITY", "2")):
    "Linear texture filtering could not be enabled"

  const imgFlags: cint = IMG_INIT_PNG
  sdlFailIf(image.init(imgFlags) != imgFlags):
    "SDL2 Image initialization failed"
  defer: image.quit()

  sdlFailIf(ttfInit() == SdlError):
    "SDL2 TTF initialization failed"
  defer: ttfQuit()

  let window = createWindow(title = "Font Rendering Test",
    x = SDL_WINDOWPOS_CENTERED, y = SDL_WINDOWPOS_CENTERED,
    w = windowSize.x, h = windowSize.y, flags = SDL_WINDOW_SHOWN or SDL_WINDOW_RESIZABLE)
  sdlFailIf window.isNil: "Window could not be created"
  # discard window.setFullscreen(SDL_WINDOW_FULLSCREEN_DESKTOP)
  defer: window.destroy()

  let renderer = window.createRenderer(index = -1, flags = Renderer_Accelerated or Renderer_PresentVsync)
  sdlFailIf renderer.isNil: "Renderer could not be created"
  defer: renderer.destroy()

  var
    scene = newScene()
    inputHandler = newInputHandler()
    fontcache = newFontCache()
    glyphcache = newGlyphCache()

  var
    text1 = newText("Hello World!", 500, 300, fontcache.get("DejaVuSans.ttf", 64), color(200, 200, 200, 255), color(255, 255, 255, 128), 5, glyphcache)
    sprite1 = newSprite(renderer.loadTexture_RW(readRW("screenshot.png"), freesrc = 1), 0, 0, 1920, 1080, 1)
    blurEffect = newBlurEffect(sprite1, renderer) 
  
  scene.add(sprite1)
  scene.add(text1)

  # scene loop, draws each frame
  var ctr:uint32 = 0
  var sum:uint32 = 0
  var x,y: cint
  while not inputHandler.inputs[Input.quit]:
    inputHandler.handleInput()
    # getMouseState(x, y)
    # text1.x = int(float(x) - text1.width / 2)
    # text1.y = int(float(y) - text1.height / 2)
    if inputHandler.inputs[Input.blur]:
      var t1 = getTicks()
      # blurEffect.apply(rect(500,500,200,200), renderer)
      blurEffect.apply(renderer)
      echo getTicks()-t1
    if inputHandler.inputs[Input.reset]:
      blurEffect.reset(renderer)
    if inputHandler.inputs[Input.left]:
      sprite1.x -= 1
    if inputHandler.inputs[Input.right]:
      sprite1.x += 1
    if inputHandler.inputs[Input.up]:
      sprite1.y -= 1
    if inputHandler.inputs[Input.down]:
      sprite1.y += 1
    var prev = getTicks()
    scene.render(renderer)
    ctr = ctr + 1
    sum = sum + getTicks() - prev
    if ctr == 10:
      setTitle(window, intToStr(int(cast[int](sum)/cast[int](ctr))))
      ctr = 0
      sum = 0

main()
