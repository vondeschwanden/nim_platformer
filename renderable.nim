import
  sdl2

type
  Renderable* = ref object of RootObj
    texture*: TexturePtr
    x*, y*, width*, height*: int
    scale*: float

# proc `x=`*(renderable: Renderable, value: int) = 
#   renderable.x = value
#   

method render*(renderable: Renderable, renderer: RendererPtr) {.base.} = 
  quit "to override!"

method is_dirty*(renderable: Renderable): bool {.base.} = 
  quit "to override!"
